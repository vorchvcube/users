import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../user/service/auth.service';
import {Router} from '@angular/router';
import {MzToastService} from 'ngx-materialize';
import {Observable} from 'rxjs';
import {User} from '../../user/models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor (
    private authService: AuthService,
    private router: Router,
    private toastService: MzToastService,
  ) { }
  currentUser: User;

  ngOnInit() {
    this.authService.getCurrentUser().subscribe(res => {
      this.currentUser = res;
      console.log(this.currentUser)
    }, err => {
      console.log(err);
    })
  }

  logout () {
    this.authService.logout().subscribe(res => {
      this.router.navigate(['/login']);
    }, err => {
      console.log(err);
      this.toastService.show(`Ошибка ${err.message}`, 4000, 'red');
    });
  }
  /*Не забыть поменять на обратную роль*/
  linkShedule () {
    if (this.currentUser.role !== "user") {
      this.router.navigate(['/dashboard/shedule']);
    } else {
      this.toastService.show('Кажется у вас недостаточно прав', 4000, 'red');
    }

  }
}

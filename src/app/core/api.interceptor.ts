import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Configuration} from '../../app.constants';
@Injectable()
export class OptionsInterceptor implements HttpInterceptor {
  constructor(private configuration: Configuration) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.url.includes(this.configuration.ServerWithApiUrl)) {
      return next.handle(req);
    }
    const authReq = req.clone({
      headers: req.headers.set('credentials', `same-origin`),
      withCredentials: true,
    });
    return next.handle(authReq);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from "../user/user.component";
import {HomeComponent} from "../home/home.component";
import {DashboardComponent} from "./dashboard.component";
import {AuthGuard} from "../core/guards/auth-guard.guard";
import { UserEditComponent } from '../user/user-edit/user-edit.component';
import {ScheduleComponent} from '../schedule/schedule.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent, pathMatch: 'full'},
      { path: 'shedule', component: ScheduleComponent, pathMatch: 'full'},
      { path: 'user', component: UserComponent, pathMatch: 'full'},
      { path: 'user/:id', component: UserEditComponent, pathMatch: 'full'},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {UserModule} from "../user/user.module";
import {DashboardComponent} from "./dashboard.component";
import {HeaderComponent} from "../core/header/header.component";
import {HomeModule} from "../home/home.module";
import {ScheduleModule} from '../schedule/schedule.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DashboardRoutingModule,
    HomeModule,
    UserModule,
    ScheduleModule
  ],
  declarations: [HeaderComponent, DashboardComponent ]
})
export class DashboardModule { }

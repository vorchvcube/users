import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../user/service/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit() {
   /* this.authService.getCurrentUser().subscribe(res => {
      this.router.navigate(['/dashboard']);
    }, err => {
      this.router.navigate(['/login']);
    });*/
  }
}

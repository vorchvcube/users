import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { LoginComponent } from './user/login/login.component';

import { AuthGuard } from './core/guards/auth-guard.guard';
/*canActivate: [AuthGuard]*/
import {NotFoundComponent} from './core/not-found/not-found.component';
const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
  {
    path: 'login',
    component: LoginComponent,
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MzCardModule, MzIconMdiModule, MzIconModule, MzInputModule, MzSpinnerModule,
  MzToastModule
} from 'ngx-materialize';
import {LoginComponent} from './user/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import { CoreModule } from './core/core.module';
import {CoreComponent} from './core/core.component';
import {AuthGuard} from "./core/guards/auth-guard.guard";
import {AuthService} from './user/service/auth.service';
import {Configuration} from "../app.constants";
import { HTTP_INTERCEPTORS , HttpClient, HttpClientModule} from '@angular/common/http';
import {UserService} from "./user/service/user.service";
import {OptionsInterceptor} from './core/api.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CoreComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    CoreModule,
    HttpClientModule,
    /*materialise*/
    MzSpinnerModule,
    MzInputModule,
    MzIconModule,
    MzIconMdiModule,
    MzToastModule,
    MzCardModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    Configuration,
    HttpClient,
    HttpClientModule,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: OptionsInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}

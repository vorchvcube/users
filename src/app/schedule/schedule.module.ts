import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ScheduleComponent } from './schedule.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxChartsModule,
  ],
  declarations: [ScheduleComponent]
})
export class ScheduleModule { }

import {Component, OnInit} from '@angular/core';
import {UserService} from '../user/service/user.service';
import {User} from '../user/models/user';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {
  view: any[] = [700, 400];
  users: User[];
  data: any[] = [
    {
      'name': 'Возраст',
      'series': [
        {
          'value': 18,
          'name': 'Aleksey'
        },
        {
          'value': 16,
          'name': 'Ivan'
        },
        {
          'value': 45,
          'name': 'Sergey'
        },
      ]
    }
  ];
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Пользователи';
  showYAxisLabel = true;
  yAxisLabel = 'Возраст';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private usersService: UserService) {
  }

  ngOnInit() {
    this.usersService.userList.subscribe(value => {
      if (value) {
        this.users = value;
      } else {
        this.usersService.getAll().subscribe(res => {
          this.users = res['colorsArray'];
          /*this.dataFraph();*/
        }, err => {
          console.log(err);
        });
      }
    });
  }

  onSelect(event) {
    console.log(event);
  }

  /*dataFraph () {
    this.users.forEach(value => {
      console.log(value);
      this.data.push(
        {
          "name": value.name,
          "value": value.group
        }
      )
    })
  }*/
}

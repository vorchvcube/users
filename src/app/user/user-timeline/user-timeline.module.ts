import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserTimelineComponent } from './user-timeline.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [UserTimelineComponent],
  exports: [UserTimelineComponent]
})
export class UserTimelineModule { }

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {MzToastService} from 'ngx-materialize';
import {AuthService} from '../service/auth.service';
import {User} from '../models/user';
import {Configuration} from '../../../app.constants';

@Component({
  selector: 'app-user-timeline',
  templateUrl: './user-timeline.component.html',
  styleUrls: ['./user-timeline.component.scss']
})
export class UserTimelineComponent implements OnInit {

  constructor(
    private usersService: UserService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private toastService: MzToastService,
    private router: Router,
    private config: Configuration
  ) { }

  userTimeLine: any;
  id: number;
  arrayMonth: any = this.config.arrayMonth;


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.userById (this.id);
  }

  userById (id) {
    this.usersService.getById(id).subscribe(res => {
      this.userTimeLine = res.monthTiming.sort(this.sDecrease);
    }, err => {
      console.log(err);
    });
  }

  sDecrease (i, ii) {
    if (i > ii)
      return -1;
    else if (i < ii)
      return 1;
    else
      return 0;
  }
}

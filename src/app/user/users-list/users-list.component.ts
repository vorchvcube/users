import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../service/user.service";
import {User} from "../models/user";
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    )
  ],
})
export class UsersListComponent implements OnInit {
  users: User[];
  currentIndex: number = 0;
  userListStorage: User[];
  total: number;
  currentType = 'first';
  groupUser = [
    {
      title: 'first',
    },
    {
      title: 'second'
    },
    {
      title: 'third'
    }
  ];
  constructor(private usersService: UserService) {
  }

  ngOnInit() {
    this.usersService.userList.subscribe(value => {
      if (value) {
        this.users = value;
      } else {
        this.usersService.getAll().subscribe(res => {
          this.users = res['colorsArray'];
          this.usersService.userListuserList.next(this.users);
          this.filterType(this.currentType);
        }, err => {
          console.log(err);
        });
      }
    });
  }

  slideNext() {
    if (this.total > 0) {
      this.currentIndex = (this.currentIndex === this.total - 1) ? 0 : this.currentIndex + 1;
    }
  }

  slidePrev() {
    if (this.currentIndex !== 0) {
      this.currentIndex = --this.currentIndex;
    }
  }
  filterType (type: string) {
    this.currentType = type;
    this.currentIndex = 0;
    let arr = this.users.filter((item) => {
      return item.group === this.currentType;
    });
    this.total = arr.length;
  }
}

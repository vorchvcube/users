import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from "./user.component";
import {UserEditComponent} from "./user-edit/user-edit.component";
import {UsersListModule} from './users-list/users-list.module';
import {UsersEditModule} from './user-edit/users-edit.module';
import {AuthGuard} from '../core/guards/auth-guard.guard';

const routes: Routes = [
  {
    path: 'user',
    component: UserComponent,
    pathMatch: 'full',
    children: [
      { path: 'edit/:id', component: UserComponent, pathMatch: 'full'},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }

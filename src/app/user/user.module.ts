import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './users-list/users-list.component';
import {UsersListModule} from "./users-list/users-list.module";
import {RouterModule} from "@angular/router";

import {UserRoutingModule} from "./user-routing.module";
import {UsersEditModule} from "./user-edit/users-edit.module";
import {UserComponent} from "./user.component";
import {UserFilterPipe} from './pipe/user-filter-pipe.pipe';
import {UserTimelineModule} from './user-timeline/user-timeline.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    UsersListModule,
    UserRoutingModule,
    UsersEditModule,
    UserTimelineModule
  ],
  declarations: [UserComponent, UsersListComponent, UserFilterPipe]
})
export class UserModule { }

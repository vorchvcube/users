import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs/index';
import { Configuration } from "../../../app.constants";
import { User } from "../models/user";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userListuserList: BehaviorSubject<any> = new BehaviorSubject(null);

  userList = this.userListuserList.asObservable();

  constructor(private configuration: Configuration,
              private http: HttpClient) {
  }

  public getAll(): Observable<any> {
    return this.http.get(`${this.configuration.ServerWithApiUrl}users`);
  }

  public getById(id: number): Observable<any> {
    return this.http.get(`${this.configuration.ServerWithApiUrl}dataById?id=${id}`);
  }

  public updateById(id: number, data): Observable<any> {
    return this.http.put(`${this.configuration.ServerWithApiUrl}editUser`, {id: id, data: data});
  }
}

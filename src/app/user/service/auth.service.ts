import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/index';
import {Configuration} from '../../../app.constants';
import {User} from '../models/user';
import {BehaviorSubject} from 'rxjs';



@Injectable()
export class AuthService {

  constructor(private configuration: Configuration,
              private http: HttpClient) {

  }

  public login(data): any {
    return this.http.post(`${this.configuration.ServerWithApiUrl}login`, data, {responseType: 'text'});
  }

  public getCurrentUser(): any {
    return this.http.get(`${this.configuration.ServerWithApiUrl}currentUser`);
  }

  public getCurrentStore(): any {
    const userString = localStorage.getItem('currentUser');
    if (!isNullOrUndefined(userString)) {
      const user: User = JSON.parse(userString);
      return true;
    } else {
      return false;
    }
  }

  public logout(): Observable<any> {
    return this.http.post(`${this.configuration.ServerWithApiUrl}logout`, null, {responseType: 'text'});
  }

}

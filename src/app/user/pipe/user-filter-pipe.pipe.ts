import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class UserFilterPipe implements PipeTransform {
  transform(items: any[], arg): any {
    if (items) {
      return items.filter(item =>
        item.group === arg
      );
    }
  }
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {UserEditComponent} from "../user-edit/user-edit.component";
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MzInputModule, MzTextareaModule} from 'ngx-materialize';
import {UserTimelineComponent} from '../user-timeline/user-timeline.component';
import {UserTimelineModule} from '../user-timeline/user-timeline.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MzInputModule,
    MzTextareaModule,
    UserTimelineModule
  ],
  exports: [],
  declarations: [UserEditComponent],
})
export class UsersEditModule { }

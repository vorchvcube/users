import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {User} from '../models/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../service/auth.service';
import {MzToastService} from 'ngx-materialize';
import {BehaviorSubject, Observable} from 'rxjs';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private usersService: UserService,
    private route: ActivatedRoute,
    private authService: AuthService,
    private toastService: MzToastService,
    private router: Router,
  ) { }
  userEditForm: FormGroup;
  id: number;
  user: User;
  userList: User[];
  currentUser: User;
  timeLine: any;

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.userById (this.id);
    this.usersService.userList.subscribe(res => {
      this.userList = res;
    });
    this.initForm();

    this.authService.getCurrentUser().subscribe(user => {
      this.currentUser = user;
    }, err =>  {
      console.log(err)
    })
  }

  userById (id) {
    this.usersService.getById(id).subscribe(res => {
      this.user = res;
      console.log(this.user);
      if (!this.user) {
        this.router.navigate(['/dashboard']);
      }
      this.userEditForm.patchValue({
        name: this.user.name,
        description: this.user.description,
      });
    }, err => {
      console.log(err);
    });
  }


  initForm() {
    this.userEditForm = this.fb.group({
      name: ['', [
        Validators.required,
      ]],
      description: ['', [
        Validators.required,
      ]],
    });
  }

  onSubmit () {
    const data = {
      name: this.userEditForm.controls.name.value,
      description: this.userEditForm.controls.description.value
    };

    if (this.userEditForm.invalid) {
      this.toastService.show('Пожалуйста заполните все поля формы', 4000, 'red');
    } else if (this.currentUser.role !== "admin") {
      this.toastService.show('К сожалению у вас недостаточно прав, для редактирования', 4000, 'red');
    } else {
      if (this.userList) {
        let index = this.userList.findIndex((item: User) => item.id === this.user.id);
        this.userList[index].name = data.name;
        this.userList[index].description = data.description;
        this.usersService.userListuserList.next(this.userList);
        this.toastService.show(`Юзер успешно отредактирован`, 4000, 'green');
      }
    }
  }

}

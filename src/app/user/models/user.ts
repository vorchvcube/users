export class User {
  id: number;
  name: string;
  description: string;
  group: string;
  login: string;
  password: string;
  role: string;
  username: string;
  monthTiming: any[];
}

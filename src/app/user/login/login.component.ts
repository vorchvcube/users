import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MzToastService} from 'ngx-materialize';
import {AuthService} from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private toastService: MzToastService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.initForm();
    this.authService.getCurrentUser().subscribe(res => {
      this.router.navigate(['/dashboard']);
    }, err => {
      this.router.navigate(['/login']);
    })
  }

  initForm() {
    this.loginForm = this.fb.group({
      login: ['', [
        Validators.required,
      ]],
      password: ['', [
        Validators.required,
      ]]
    });
  }

  onSubmit() {
    const data = {
      login: this.loginForm.controls.login.value,
      password: this.loginForm.controls.password.value
    };
    if (this.loginForm.invalid) {
      this.toastService.show('Пожалуйста заполните все поля формы', 4000, 'red');
    } else {
      this.authService.login(data).subscribe(res => {
        this.router.navigate(['/dashboard']);
        this.toastService.show(`Добро пожаловать`, 4000, 'green');
      }, err => {
        this.toastService.show(`Ошибка ${err.message}`, 4000, 'red');
      });
    }
  }
}


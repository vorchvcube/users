import {Injectable} from '@angular/core';

@Injectable()
export class Configuration {
    public Server = 'http://localhost:8888/';
    public ServerWithApiUrl = this.Server;

    public arrayMonth = [
      {
        id: 1,
        title: 'Январь'
      },
      {
        id: 2,
        title: 'Февраль'
      },
      {
        id: 3,
        title: 'Март'
      },
      {
        id: 4,
        title: 'Апрель'
      },
      {
        id: 5,
        title: 'Май'
      },
      {
        id: 6,
        title: 'Июнь'
      },
      {
        id: 7,
        title: 'Июль'
      },
      {
        id: 8,
        title: 'Август'
      },
      {
        id: 9,
        title: 'Сентябрь'
      },
      {
        id: 10,
        title: 'Октябрь'
      },
      {
        id: 11,
        title: 'Ноябрь'
      },
      {
        id: 12,
        title: 'Декабрь'
      }
    ]
}

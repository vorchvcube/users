import { browser, by, element } from 'protractor';

export class Dashboard {
  navigateTo() {
    return browser.get('/dashboard/home');
  }

  getParagraphText() {
    return element(by.css('.title-home')).getText();
  }
}

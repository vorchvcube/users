import {DashboardUser} from './dashboard-user.po';


describe('Проверка страницы юзеров', () => {
  let page: DashboardUser;

  beforeEach(() => {
    page = new DashboardUser();
  });

  it('Заголовок на странице пользователей', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Список пользователей');
  });


});

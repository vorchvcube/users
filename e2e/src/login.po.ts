import { browser, by, element } from 'protractor';

export class LoginPage {
  private credentias = {
    username: 'ivanpro',
    password: 'digitalwand'
  };
  navigateTo() {
    return browser.get('/login');
  }

  fillCredentials(credentias: any = this.credentias) {
    element(by.css('.login')).sendKeys(credentias.username);
    element(by.css('.password')).sendKeys(credentias.password);
    element(by.css('.btn')).click();
  }


  getPageTitleText() {
    return element(by.css('.title-login')).getText();
  }

  getErrorMessage() {
    return element(by.css('.toast')).getText();
  }

}

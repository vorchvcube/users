import {LoginPage} from './login.po';
import {Dashboard} from './dashboard.po';



describe('Проверка страницы логина', () => {
  let page: LoginPage;
  let publicPage: Dashboard;

  const wrongCredentias = {
    username: 'ivanpro',
    password: 'digital12123123w1and'
  };

  beforeEach(() => {
    page = new LoginPage();
    publicPage = new Dashboard();
  });

  it('Если данные некорректны, то появляется ошибка', () => {
    page.navigateTo();
    page.fillCredentials(wrongCredentias);
    expect(page.getPageTitleText()).toEqual('Авторизация');
    expect(page.getErrorMessage()).toEqual('Ошибка Http failure response for http://localhost:8888/login: 404 Not Found');
  });

  it('Если данные корректны, то отправляем на страницу юзеров и проверяем, что все успешно', () => {
    page.navigateTo();
    page.fillCredentials();
    expect(publicPage.getParagraphText()).toEqual('Главная страница');
  });


});

import { browser, by, element } from 'protractor';

export class DashboardUser {
  navigateTo() {
    return browser.get('/dashboard/user');
  }

  getParagraphText() {
    return element(by.css('h4')).getText();
  }
}

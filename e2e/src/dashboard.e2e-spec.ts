import {Dashboard} from './dashboard.po';


describe('Проверка страницы юзеров', () => {
  let page: Dashboard;

  beforeEach(() => {
    page = new Dashboard();
  });

  it('Заголовок на главной странице', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Главная страница');
  });


});

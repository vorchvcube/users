import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Заголовок на странице пользователей', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Список пользователей');
  });


});
